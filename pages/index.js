import Image from 'next/image';
import Link from 'next/link';
import Layout from '../components/layout'
import styles from '../styles/Home.module.css';



const Home = () => {
  return (
    <Layout>
        
          <main className={`container ` + styles.main}>
            <div className='pb-20'>
              <h1 className='text-6xl text-blue-500 font-extrabold pt-32'>Get paid <br></br>easily & scale</h1>
              <p className="text-lg text-blue-500 pt-10 mb-10 w-80">Paylink, an e-commerce platform that empowers businesses 
                to showcase their products and services online and get 
                paid instantly through multiple channels.
              </p>
              <Link href="/">
                  <a  className="text-sm font-small text-white bg-orange rounded-full px-14 py-5">
                      Get Started
                  </a>
              </Link>
              
              <p className="text-sm text-blue-500 pt-10">Already registered ?
              {' '}
                <Link href="/">
                  <a  className="text-sm font-small text-orange underline">
                      Sign in here
                  </a>
              </Link></p>
            </div>
          </main>

          <footer className='container bg-gray-100 py-8'>
            <div className="flex justify-between">
              <div>
                <h5 className="text-sm text-blue-500 font-medium pb-4">CONTACT US </h5>
                <p className="text-xs text-blue-500"><b>Address: </b>Plot B22 Chief Yesufu Abiodun Oniru Road, Oniru, Lagos, Nigeria.</p>
                <p className="text-xs text-blue-500"><b>Support: </b>+234-1-6367000 | +234700-7877678 | support@paylink.ng</p>
                <p className="text-xs text-orange pt-4">Powered By</p>
              </div>
              <div className="">
                <p className="text-xs text-blue-500">
                  <Link href='/'>
                    <a>PRIVACY POLICY</a>
                  </Link>
                </p>
                <p className="text-xs text-blue-500">
                  <Link href='/'>
                    <a>TERMS & CONDITION</a>
                  </Link>
                </p>
                <div className="justify-between pt-3">
                  <i className="rounded-full bg-blue-500 pt-2.5 p-1 mr-1.5 pb-0.5">
                    <Image
                      src="/images/facebook.svg" // Route of the image file
                      alt="Your Name"
                      height={22}
                      width={22}
                    />
                  </i>
                  <i className="rounded-full bg-blue-500 pt-2.5 p-1 mr-1.5 pb-0.5">
                    <Image
                        src="/images/twitter.svg" // Route of the image file
                        alt="Your Name"
                        height={22}
                        width={22}
                        className="rounded-full"
                    />
                  </i>  
                  <i className="rounded-full bg-blue-500 pt-2.5 p-1 pb-0.5">
                    <Image
                        src="/images/instagram.svg" // Route of the image file
                        alt="Your Name"
                        height={22}
                        width={22}
                    />
                  </i>  
                </div>
              </div>
            </div>
          </footer>
    </Layout>
  )
}

export default Home;

