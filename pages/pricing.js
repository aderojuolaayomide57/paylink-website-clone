import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout from '../components/layout';
import Maincontainer from '../components/main-container';

const Pricing = () => {
    return (
        <Layout>
            <Head>
                <title>Pricing</title>
            </Head>
            <Maincontainer>
                <div className="grid grid-cols-1 divide-y divide-blue-500 p-14">
                    <div>
                        <h1 className='text-sm text-center'>PRICING</h1>
                        <h1 className="text-7xl text-blue-800 text-center font-bold pt-5 pb-5">1%</h1>
                        <p className="text-sm text-center">per transaction <br></br>
                            Capped at NGN 2,000
                        </p>
                    </div>
                    <div className="mt-5 p-8">
                        <p className="text-sm text-center pb-5">Receive funds from your Customers Via:</p>
                        <p className="text-md text-center font-small">USSD</p>
                        <p className="text-md text-center font-small">Debit & Credit Card</p>
                        <p className="text-md text-center font-small">Internet Banking</p>
                        <p className="text-dm text-center font-small">Wallets</p>
                    </div>
                    <div className="pt-5">
                        <p className="text-sm text-center">Please note that when foreign cards are used, an additional 3% 
                            charge is applicable
                        </p>
                        <p className="text-sm text-center pt-10 pb-16">Prices are VAT exclusive</p>
                        <p className="text-center"><Link href="/">
                            <a  className="text-sm font-small text-white bg-orange rounded-full px-11 py-8">
                                Get Started
                            </a>
                        </Link>
                        </p>
                    </div>
                </div>
            </Maincontainer>
            
        </Layout>
    )
}


export default Pricing;
