import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout from '../components/layout';
import Maincontainer from '../components/main-container';


const Integration = () => {
    return (
        <Layout>
            <Head>
                <title>Integration</title>
            </Head>
            <Maincontainer>
                <div className="p-8 text-gray-500">
                    <h1 className='text-2xl text-blue-800 font-bold pb-2'>Paylink Checkout JS Library</h1>
                    <p className="text-sm leading-6 pb-6">
                        Paylink is a platform that allows you receive 
                        payments and sell items without giving out your 
                        account details. We offer APIs to make integration 
                        with Paylink a breeze
                    </p>
                    <h1 className='text-2xl text-blue-800 font-bold pb-2'>Usage</h1>
                    <p className="text-sm leading-6">
                        You will need to include the Paylink Checkout Library file in your page
                    </p>
                </div>
            </Maincontainer>
            
        </Layout>
    )
}


export default Integration;
