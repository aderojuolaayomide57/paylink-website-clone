import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout from '../components/layout';
import Maincontainer from '../components/main-container';

const Testimonials = () => {
    return (
        <Layout>
            <Head>
                <title>Testimonials</title>
            </Head>
            <Maincontainer>
            <h1 className='text-sm text-center text-blue-800'>Testimonials</h1>
                    <div>
                        <h1 className='text-sm text-center'>PRICING</h1>
                        <h1 className="text-7xl text-blue-800 text-center font-bold pt-5 pb-5">1%</h1>
                        <p className="text-sm text-center">per transaction <br></br>
                            Capped at NGN 2,000
                        </p>
                    </div>
            </Maincontainer>
            
        </Layout>
    )
}


export default Testimonials;
