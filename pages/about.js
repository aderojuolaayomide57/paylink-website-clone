import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout from '../components/layout';
import Maincontainer from '../components/main-container';


const About = () => {
    return (
        <Layout>
            <Head>
                <title>About Paylink</title>
            </Head>
            <Maincontainer>
                <h1 className='text-sm text-center text-blue-800'>About Paylink</h1>
                <div className="p-8">
                    <p className="text-sm">
                        Paylink is a payment solution for you to 
                        receive money INSTANTLY into your bank account 
                        from Anyone, Anywhere, Anytime.
                    </p>
                    <br></br>
                    <p className="text-sm">
                        Whether you run your business on Instagram, Twitter, Facebook, 
                        Snapchat or you have a website which customers visit to pay you, 
                        this solution is a perfect fit for you. All you have to do is get a link 
                        customised to your business or personal profile name and logo, and 
                        send to your customers for payment.
                    </p>
                    <br></br>
                    <p className="text-sm">
                        With Paylink, the era of exposing yourself to online fraud by 
                        sending your bank account details to people you have most likely never 
                        met for payment on the internet is over. Paylink configures your bank account 
                        and hides it from public view.
                    </p>
                    <br></br>
                    <br></br>
                    <p className="text-sm">
                        With Paylink, you can easily:<br></br>
                        - Receive Payments for Goods & Services<br></br>
                        - Receive Donations & Fees (Church, Charity, School etc)<br></br>
                        - Receive Events Contributions (Aso Ebi, Gele, Fila etc)<br></br>
                        - Receive Payments instantly
                    </p>
                    <br></br>
                    <br></br>
                    <p className="text-sm">
                        Using paylink also allows you to conveniently receive payments 
                        through various channels, including:<br></br>
                        - USSD<br></br>
                        - Debit/Credit Card<br></br>
                        - Internet Banking<br></br>
                        - Wallets
                    </p>
                    <br></br>
                    <p className="text-sm">
                        Enjoy a whole new world of fast and efficient funds collection <br></br>
                        
                    </p>
                </div>
            </Maincontainer>
            
        </Layout>
    )
}


export default About;
