import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import Layout from '../components/layout';
import Maincontainer from '../components/main-container';


const Faq = () => {
    return (
        <Layout>
            <Head>
                <title>FAQ</title>
            </Head>
            <Maincontainer>
                <h1 className='text-2xl text-center font-bold pb-2'>Got questions?</h1>
                <p className="text-sm leading-6 pb-6 text-center">Perfect, we've got answers</p>
                <div className="grid grid-cols-1 p-5">
                    <div>
                        <h1 className="text-lg font-bold pt-3 pb-3 border-b-2 border-grey-200">About Paylink ?</h1>
                        <p className="text-sm p-4 bg-gray-100 text-gray-600">
                            Paylink is a web-based solution that helps Micro, 
                            Small and Medium-scale Enterprises (MSMEs) as well as 
                            religious organisations, not-for-profit outfits, social media sellers, 
                            freelancers, individuals and many more to swiftly receive payments 
                            from anywhere using a customized link
                        </p>
                    </div>
                    <div>
                        <h1 className="text-lg font-bold pt-3 pb-3 border-b-2 border-grey-200">Using Paylink ?</h1>
                        <p className="text-sm p-4 bg-gray-100 text-gray-600">
                            Paylink is a web-based solution that helps Micro, 
                            Small and Medium-scale Enterprises (MSMEs) as well as 
                            religious organisations, not-for-profit outfits, social media sellers, 
                            freelancers, individuals and many more to swiftly receive payments 
                            from anywhere using a customized link
                        </p>
                    </div>
                    <div>
                        <h1 className="text-lg font-bold pt-3 pb-3 border-b-2 border-grey-200">Charges/Fees ?</h1>
                        <p className="text-sm p-4 bg-gray-100 text-gray-600">
                            Paylink is a web-based solution that helps Micro, 
                            Small and Medium-scale Enterprises (MSMEs) as well as 
                            religious organisations, not-for-profit outfits, social media sellers, 
                            freelancers, individuals and many more to swiftly receive payments 
                            from anywhere using a customized link
                        </p>
                    </div>
                    <div>
                        <h1 className="text-lg font-bold pt-3 pb-3 border-b-2 border-grey-200">Customer Support </h1>
                        <p className="text-sm p-4 bg-gray-100 text-gray-600">
                            Paylink is a web-based solution that helps Micro, 
                            Small and Medium-scale Enterprises (MSMEs) as well as 
                            religious organisations, not-for-profit outfits, social media sellers, 
                            freelancers, individuals and many more to swiftly receive payments 
                            from anywhere using a customized link
                        </p>
                    </div>
                    
                </div>
            </Maincontainer>
            
        </Layout>
    )
}


export default Faq;
