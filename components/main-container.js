import Link from 'next/link';
import styles from '../styles/layout.module.css';
import Head from 'next/head';



const Maincontainer = ({children}) => {
    return( 
        <div className="mx-auto shadow-lg p-3 pt-10 pb-10 rounded-lg" style={{maxWidth: 500}}>
            {children}
        </div>
    )
}

export default Maincontainer;